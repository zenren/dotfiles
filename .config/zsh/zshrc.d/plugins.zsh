# Theme
source ~/.local/share/zsh/plugins/powerlevel10k/powerlevel10k.zsh-theme
# Replace zsh completion with fzf
#source ~/.local/share/zsh/plugins/fzf-tab-git/fzf-tab.zsh
source ~/.local/share/zsh/plugins/fzf-tab-bin-git/fzf-tab.zsh
# Use syntax highlighting
source ~/.local/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
source ~/.local/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
